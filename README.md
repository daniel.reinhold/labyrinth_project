# Labyrinth Projekt

### Webanwendung zur Visualisierung der Lösung eines Labyrinths nach dem Zufallsprinzip

---

-  ## Inhaltsverzeichnis
   1. Generieren der Entwicklerdokumentation
   2. Anwendungsinitialisierung
   3. Anwendung
      1. Übersicht
      2. Theorie zur Generierung des Labyrinths
      3. Praktische Umsetzung der Generierung des Labyrinths
      4. Theorie zur Lösung des Labyrinths
      5. Praktische Umsetzung der Lösung des Labyrinths
   4. Schlusswort

- ## 1. Generieren der Entwicklerdokumentation
  - Für die Entwicklerdokumentation wurde [JSDoc](https://jsdoc.app/) benutzt.
    - Vorraussetzung: [NPM](https://www.npmjs.com/) muss installiert sein
    - Installation von JSDoc:
      - `npm i -g jsdoc`
    - Zur Generierung der Dokumentation muss folgender Befehl ausgeführt werden:
      - `docs/generate.cmd`
    - Die Dokumentation wird dann generiert und unter `docs/html` gesichert
- ## 2. Anwendungsinitialisierung:
  - Es sind keine Schritte notwendig, außer die Datei `index.html` zu öffnen
- ## 3. Anwendung:
  1. ### Übersicht:
    - <img alt="Anwendungsübersicht" src="docs/img/app_overview.png" width="500px">
    - Im **Zentrum** des Viewports befindet sich die **Ansicht für das Labyrinth**
    - **Unterhalb** der Ansicht für das Labyrinth befindet sich die **Benutzeroberfläche, mit welcher der Nutzer interagieren** kann.
      - Diese Benutzeroberfläche beinhaltet **zwei Textfelder**, mit der die **Höhe**, sowie die **Breite** für das zu generirende Labyrinth angegeben werden kann. Die Höhe und die Breite betragen standardmäßig 20
      - **Unter den Textfeldern** befindet sich ein **Slider**, mit der die **Schwierigkeit für die Lösung des Labyrinths** geändert werden kann. Die Farbe des Fortschrittsbalkens ändert sich je nach Schwierigkeitsgrad
      - Auf der **rechten Seite** der Benutzeroberfläche befindet sich eine **Knopf**, mit welcher das **Labyrinth generiert** werden kann
    - Sollten ungültige Werte eingegeben werden, so wird eine Meldung im Oberen Bereich des Viewports angezeigt, außerdem wird das betreffende **Textfeld mit roter Farbe markiert**
      - Ungültige Werte für beide Textfelder sind:
        - Zahlen die **kleiner oder gleich null** sind
        - Alle Symbole **außer Zahlen**
        - **Gar keine** Eingabe
      - Beispiel: 
        - <img alt="Anwendungsübersicht mit Fehlerhaften eingaben" src="docs/img/app_overview_with_errors.png" width="500px">
    - Ein Generiertes Labyrinth sieht folgendermaßen aus:
      - <img alt="Anwendungsübersicht mit generiertem Labyrinth" src="docs/img/app_overview_with_labyrinth.png" width="500px">
  - 2. ### Theorie zur Generierung des Labyrinths
    - Erzeugung des Labyrinths:
      - Für jede erzeugte Kachel wird eine zufällige Gleitkommezahl zwischen 0 und 1 generiert. Die Schwierigkeitsstufe ist auch eine Gleitkommazahl zwischen 0 und 1.
      - Ist die zufällige generierte Zahl kleiner oder gleich der Schwierigkeitsstufe, so wird die Kachel als "begehbar" markiert, ist die Zahl größer als der Schwierigkeitsgrad, wird sie als "nicht begehbar gekennzeichnet"
      - Nachdem das Labyrinth generiert wurde, wird jede Kachel noch einmal betrachtet und bekommt für jede Seite eine `0` (für eine begehbare anliegende Kachel) oder eine `1` (für eine nicht begehbare anliegende Kachel) zugewisen.
			*
			<table>
				<thead>
					<tr>
						<th colspan="4">Stellenwerte der Dualzahl</th>
					</tr>
					<tr>
						<th>Erste Stelle</th>
						<th>Zweite Stelle</th>
						<th>Dritte Stelle</th>
						<th>Vierte Stelle</th>
					</tr>
					<tbody>
						<tr>
							<td>Obere anliegende Kachel</td>
							<td>Rechte anliegende Kachel</td>
							<td>Untere anliegende Kachel</td>
							<td>Linke anliegende Kachel</td>
						</tr>
					</tbody>
				</thead>
			</table>
				
      - Daraus ergibt sich eine **Dualzahl**, die mindetens den Wert `0000` und maximal den Wert `1111` haben kann
			- Am Ende wird diese **Dualzahl** in eine **Dezimalzahl** umgewandelt, mit der **eindeutig bestimmtbar** ist, an **welchen Seiten** der Kachel eine **begehbare** und an welcher Seite eine **nicht begehbare** Kachel anliegt
    	- Beispiele: (Bezieht sich auf die Kachel in der Mitte)
			- <table>
					<thead>
						<tr>
							<th>Bild</th>
							<th>Dualzahl</th>
							<th>Dezimalzahl</th>
						</tr>
						<tbody>
							<tr>
								<td>
									<img src="docs/img/tiles/tile_1.png" width="75px">
								</td>
								<td>1111</td>
								<td>15</td>
							</tr>
							<tr>
								<td>
									<img src="docs/img/tiles/tile_2.png" width="75px">
								</td>
								<td>0111</td>
								<td>7</td>
							</tr>
							<tr>
								<td>
									<img src="docs/img/tiles/tile_3.png" width="75px">
								</td>
								<td>1001</td>
								<td>9</td>
							</tr>
						</tbody>
					</thead>
			</table>