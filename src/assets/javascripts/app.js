App.ready(() => {
	const btnSubmit = App.select('#btn-submit');
	const fieldWidth = App.select('#labyrinth-width');
	const fieldHeight = App.select('#labyrinth-height');
	
	const sliderDifficulty = App.select('#labyrinth-difficulty');
	const minDifficulty = parseFloat(sliderDifficulty.getAttribute('min'));
	const maxDifficulty = parseFloat(sliderDifficulty.getAttribute('max'));

	const labyrinth = App.select('#labyrinth');
	const containerErrors = App.select('#errors');
	
	const alert = (message) => `<div class="alert">${message}</div>`;

	const COLOR_DIFFICULTY_LOW 		= '#28a745';
	const COLOR_DIFFICULTY_MIDDLE = '#ffc107';
	const COLOR_DIFFICULTY_HIGH 	= '#dc3545';

	sliderDifficulty.on('input', function() {
		let difficulty = parseFloat(sliderDifficulty.val());
		let color = null;

		switch (true) {
			case difficulty <= 0.25:
				color = COLOR_DIFFICULTY_LOW;
				break;
			case difficulty <= 0.8:
				color = COLOR_DIFFICULTY_MIDDLE;
				break;
			default:
				color = color = COLOR_DIFFICULTY_HIGH;
				break;
		}


		if (App.isChrome())
			document.documentElement.style.setProperty(
				'--progressbar-color-chrome', 
				`linear-gradient(to right, ${color} 0%, ${color} ${sliderDifficulty.val() * 100}%, #c1c7cc ${sliderDifficulty.val() * 100}%, #c1c7cc 100%)`);
		else
			document.documentElement.style.setProperty('--progressbar-progress-color', color);
	});

	const generateLabyrinth = (width, height, percentWalkable) => {
		labyrinth
			.removeClass(['border-dark', 'centered'])
			.empty();

		labyrinth.append
		(
			'<table id="table-labyrinth">' +
			'  <tbody></tbody>' +
			'</table>'
		);

		/* Native JavaScript is better in this case */
		const table = document.querySelector("#table-labyrinth tbody");
		let tableData = [];

		for (let row = 0; row < height; row++) {
			let tableRow = document.createElement('tr');
			if (tableData[row] === undefined)
				tableData.push([]);

			for (let col = 0; col < width; col++) {
				let tableCell = document.createElement('td');

				//tableCell.innerHTML = `[${row}][${col}]`

				if (Math.random() <= percentWalkable) {
					tableCell.classList.add('tile-walkable');
					tableData[row][col] = 0;
				} else {
					tableCell.classList.add('tile-not-walkable');
					tableData[row][col] = 1;
				}

				tableRow.appendChild(tableCell);
			}
			table.appendChild(tableRow);
		}

		let isWalkable = (row, col) => {
			if (tableData[row] === undefined || tableData[row][col] === undefined)
				return false;
			return tableData[row][col] === 0;
		}

		let finishedLabyrinthData = [];

		for (let row = 0; row < height; row++) {
			if (finishedLabyrinthData[row] === undefined)
				finishedLabyrinthData.push([]);

			for (let col = 0; col < width; col++) {
				let binary = '';

				binary += isWalkable(row - 1, col) ? '0' : '1';
				binary += isWalkable(row, col + 1) ? '0' : '1';
				binary += isWalkable(row + 1, col) ? '0' : '1';
				binary += isWalkable(row, col - 1) ? '0' : '1';

				finishedLabyrinthData[row][col] = parseInt(binary, 2);
			}	
		}

		console.log(finishedLabyrinthData);
	}
	
	btnSubmit.on(EVENTS.click, () => {
		containerErrors.empty();
		fieldWidth.removeClass('error');
		fieldHeight.removeClass('error');

		let errorsFound = false;
		
		let fieldWidthEmpty 	= fieldWidth.isEmpty();
		let fieldHeightEmpty 	= fieldHeight.isEmpty();
		let width 						= parseInt(fieldWidth.val());
		let height 						= parseInt(fieldHeight.val());

    /* #region Input Validations */

		if (fieldWidthEmpty) {
			containerErrors
				.removeClass('d-none')
				.append(alert("Bitte geben Sie eine Breite für das Labyrinth an."));

			fieldWidth.addClass('error');

			errorsFound = true;
		}

		if (fieldHeightEmpty) {
			containerErrors
				.removeClass('d-none')	
				.append(alert("Bitte geben Sie eine Höhe für das Labyrinth an."));

			fieldHeight.addClass('error');

			errorsFound = true;
		}

		if (isNaN(width) && !fieldWidthEmpty) {
			containerErrors
				.removeClass('d-none')	
				.append(alert("Bitte geben Sie eine Zahl als Höhe für das Labyrinth an."));

			fieldWidth.addClass('error');

			errorsFound = true;
		}

		if (isNaN(height) && !fieldHeightEmpty) {
			containerErrors
				.removeClass('d-none')	
				.append(alert("Bitte geben Sie eine Zahl als Höhe für das Labyrinth an."));

			fieldHeight.addClass('error');

			errorsFound = true;
		}

		if (width < 1) {
			containerErrors
				.removeClass('d-none')	
				.append(alert("Bitte geben Sie für die Breite des Labyrinthes eine Zahl, die größer als eins ist, an."));

			fieldWidth.addClass('error');
			
			errorsFound = true;
		}

		if (height < 1) {
			containerErrors
				.removeClass('d-none')	
				.append(alert("Bitte geben Sie für die Höhe des Labyrinthes eine Zahl, die größer als eins ist, an."));

			fieldHeight.addClass('error');
			errorsFound = true;
		}

		if (!errorsFound) {
			containerErrors.addClass('d-none');

			generateLabyrinth(width, height, 1.0 - parseFloat(sliderDifficulty.val()));
		} else {
			labyrinth.empty().addClass(['border-dark', 'centered']).append('<h1>Bitte korrigieren Sie ihre Fehler</h1>');
		}

    /* #endregion */

	});

});