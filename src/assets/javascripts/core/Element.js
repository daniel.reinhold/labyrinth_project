/**
 * @description HTML-Elements, for which a value can be determined
 */
const ELEMENTS_WITH_VALUE = [
	"INPUT",
	"SELECT"
];

/**
 * @author Daniel Reinhold
 * @description This class represents an HTML-Element and provides helpful methods to work with it
 */
class AppElement {

	/**
	 * @author Daniel Reinhold
	 * @description Creates a new AppElement for the passed element
	 * @param {Object} element The HTML-Element for which the AppElement should get created
	 */
	constructor(element) {
		this.element = element;

		if (ELEMENTS_WITH_VALUE.includes(this.element.tagName)) {
			this.element.addEventListener('change', (event) => {
				this.element.value = event.target.value;
			});
		}
	}

	/** 
	 * @author Daniel Reinhold
	 * @description Adds one or more classes to the AppElement
	 * @param {(String|String[])} className The class as String or an Array of classes which should get added to the AppElement
	 * @param {Boolean} returnSelf A boolean which determines if the AppElement is returned when the method gets called or not
	 * @return {AppElement} The AppElement for which this methods is being called, if returnSelf is set to true
	 */
	addClass(className, returnSelf = true) {
		if (className instanceof Array)
			className.forEach((value, _) => this.element.classList.add(value));
		else
			this.element.classList.add(className);

		if (returnSelf)
			return this;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Removes one or more classes from the AppElement
	 * @param {(String|String[])} className The class as String or an Array of classes which should get added to the AppElement
	 * @param {Boolean} returnSelf A boolean which determines if the AppElement is returned when the method gets called or not
	 * @return {AppElement} The AppElement for which this methods is being called, if returnSelf is set to true
	 */
	removeClass(className, returnSelf = true) {
		if (className instanceof Array)
			className.forEach((value, _) => this.element.classList.remove(value));
		else
			this.element.classList.remove(className);

		if (returnSelf)
			return this;		
	}

	/**
	 * @author Daniel Reinhold
	 * @description Empties the AppElement
	 * @param {Boolean} returnSelf A boolean which determines if the AppElement is returned when the method gets called or not
	 * @return {AppElement} The AppElement for which this methods is being called, if returnSelf is set to true
	 */
	empty(returnSelf = true) {
		this.element.innerHTML = '';

		if (returnSelf)
			return this;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Adds an EventListener to the AppElement
	 * @param {String} event The event for which the EventListener should get added
	 * @param {Function} callback The callback for the EventHandler
	 * @param {Boolean} returnSelf A boolean which determines if the AppElement is returned when the method gets called or not
	 * @return {AppElement} The AppElement for which this methods is being called, if returnSelf is set to true
	 */
	on(event, callback, returnSelf = true) {
		this.element.addEventListener(event, callback);

		if (returnSelf)
			return this;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Gets the value of the AppElement 
	 * @return {String|undefined} The value of the AppElement when it is any of the Elements in ELEMENTS_WITH_VALUE, undefined otherwise
	 */
	val() {
		if (ELEMENTS_WITH_VALUE.includes(this.element.tagName)) {
			return this.element.value;
		}

		return undefined;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Checks if the value of the AppElement is Empty
	 * @return {Boolean|undefined} true if the AppElement is empty, false otherwise. Returns undefined if the AppElement is not any of the Elements in ELEMENTS_WITH_VALUE
	 */
	isEmpty() {
		if (ELEMENTS_WITH_VALUE.includes(this.element.tagName)) {
			return this.element.value.toString().replaceAll(' ', '').length === 0;
		}

		return undefined;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Adds content to the AppElement
	 * @param {String|Element} content The content which should get added to the AppElement
	 * @return {AppElement} The AppElement for which this methods is being called, if returnSelf is set to true
	 */
	append(content, returnSelf = true) {
		if (content instanceof String || typeof content === 'string') {
			this.element.appendChild(
				document.createRange().createContextualFragment(content)
			);
		}

		if (returnSelf)
			return this;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Adds style to the AppElement
	 * @param {Object} style An object of styles which should get added to the AppElement
	 * @return {AppElement} The AppElement for which this methods is being called, if returnSelf is set to true
	 */
	style(styles, returnSelf = true) {
		if (styles instanceof Object || typeof styles === 'object') {
			for (const [key, value] of Object.entries(styles)) {
				this.element.style[key] = value;
			}
		}

		if (returnSelf)
			return this;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Gets a Attribute of an Element
	 * @param {String} attributeName The name of the Attribut which should get returned
	 * @return {String} The value of the attribute
	 */
	getAttribute(attributeName) {
		return this.element.getAttribute(attributeName);
	}

}

/**
 * @author Daniel Reinhold
 * @description This class represents multiple HTML-Elements and provides helpful methods to work with it
 */
class AppElementCollection {
	
	/**
	 * @author Daniel Reinhold
	 * @description Creates a new AppElementCollection for the passed elements
	 * @param {Object[]} elements The HTML-Elements for which the AppElementCollection should get created
	 */
	constructor(elements) {
		this.elements = elements;
	}

	/** 
	 * @author Daniel Reinhold
	 * @description Adds one or more classes to the AppElementCollection
	 * @param {(String|String[])} className The class as String or an Array of classes which should get added to the AppElementCollection
	 * @param {Boolean} returnSelf A boolean which determines if the AppElementCollection should get returned when the method gets called or not
	 * @return {AppElementCollection} The AppElementCollection for which this methods is being called
	 */
	addClass(className) {
		this.elements.forEach((element, _) => element.addClass(className, false));

		return this;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Removes one or more classes from the AppElementCollection
	 * @param {(String|String[])} className The class as String or an Array of classes which should get added to the AppElementCollection
	 * @return {AppElementCollection} The AppElementCollection for which this methods is being called
	 */
	removeClass(className) {
		this.elements.forEach((element, _) => element.removeClass(className, false));	

		return this;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Adds an EventListener to the AppElementCollection
	 * @param {String} event The event for which the EventListener should get added
	 * @param {Function} callback The callback for the EventHandler
	 * @return {AppElementCollection} The AppElementCollection for which this method is being called
	 */
	on(event, callback) {
		this.elements.forEach((element, _) => element.on(event, callback, false));

		return this;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Adds content to the AppElementCollection
	 * @param {String|Element} content The content which should get added to the AppElement
	 * @return {AppElementCollection} The AppElementCollection for which this method is being called
	 */
	append(content) {
		this.elements.forEach((element, _) => element.append(content, false));	

		return this;
	}

}