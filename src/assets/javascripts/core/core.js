const EVENTS = {
	loaded: {
		html: "DOMContentLoaded",
		full: "loaded"
	},
	click: 'click'
}

const ELEMENTS_ONLY_ONCE_IN_DOM = [
	'html',
	'head',
	'body'
]

const config = {
	pageLoaded: false
};

const uInt8Array = new Uint8Array(1);

/**
 * @author Daniel Reinhold
 * @description This Constant provides some useful functions for the app
 */
class App {

	/**
	 * @author Daniel Reinhold 
	 * @description This function is called at the beginning of each script to wait until the HTML Content has been loaded and parsed
	 * @throws TypeError if the parameter callback isn't a function
	 * @param {Function} callback The callback for the DOMContentLoaded event
	 */
	static ready(callback) {
		if (callback instanceof Function || typeof callback === 'function') {
			document.addEventListener(EVENTS.loaded.html, callback);
			config.pageLoaded = true;
		} else {
			throw new TypeError("Parameter callback has to be a function.");
		}
	}

	/**
	 * @author Daniel Reinhold 
	 * @description This function determines if the app is accessed via a Chromium dependent Browser or not (important for some css rules)
	 * @return true if the browser depends on Chrome, false otherwise
	 */
	static isChrome() {
		return window.chrome !== undefined;
	}

	/**
	 * @author Daniel Reinhold
	 * @description Selects elements by ID, Classname or Tagname
	 * @param {string} selector The selector of the element(s) which should to be selected
	 * @throws PageNotLoadedError if the page hasn't been fully loaded
	 * @throws ElementNotFoundError if not matching element was found
	 * @returns {(AppElement|AppElementCollection)} Selected Element(s)
	 */
	static select(selector = '*') {
		if (!config.pageLoaded)
			throw new PageNotLoadedError();

		if (selector.substr(0, 1) === '#') {
			return new AppElement(document.querySelector(selector));
		} 

		for (let i = 0; i < ELEMENTS_ONLY_ONCE_IN_DOM.length; i++) {
			if (ELEMENTS_ONLY_ONCE_IN_DOM[i].includes(selector)) {
				return new AppElement(document.querySelector(selector));
			}
		}

		let items = [];
		document.querySelectorAll(selector).forEach((element, _) => items.push(new AppElement(element)));

		if (items.length === 0)
			throw new ElementNotFoundError();

		return new AppElementCollection(items);
	}

	static random() {
		return window.crypto.getRandomValues(uInt8Array)[0];
	}

};