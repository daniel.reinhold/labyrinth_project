/**
 * @author Daniel Reinhold
 * @description The Error which gets thrown, when an element wasn't found
 */
class ElementNotFoundError extends Error {
	constructor() {
		super("For this selector no element has been found.")
	}
}

/**
 * @author Daniel Reinhold
 * @description The Error which gets thrown, when the page hasn't been fully parsed
 */
class PageNotLoadedError extends Error {
	constructor() {
		super("Page hasn't been fully loaded");
	}
}